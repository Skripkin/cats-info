import { Controller, Get, Query, Body, Post } from '@nestjs/common';

import { CatsService } from './cats.service';
import { Cat } from './entities/cats.entity';
import { NewCatDto } from './new-cat-dto.entity';

@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @Get()
  getCats(
    @Query('attribute') attribute: string,
    @Query('order') order: string,
    @Query('offset') offset: number,
    @Query('limit') limit: number,
  ) {
    return this.catsService.getCats(attribute, order, offset, limit);
  }

  @Get('color-info')
  async getColorInfo() {
    return this.catsService.getColorCounts();
  }

  @Post('add-cat')
  async addCat(@Body() newCatData: NewCatDto): Promise<Cat> {
    return this.catsService.addCat(newCatData);
  }
}
