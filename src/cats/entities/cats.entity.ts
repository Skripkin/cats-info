import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

import { CatColorEnum } from '../new-cat-dto.entity';

@Entity()
export class Cat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  name: string;

  @Column({
    type: 'enum',
    enum: [
      'black',
      'white',
      'black & white',
      'red',
      'red & white',
      'red & black & white',
    ],
  })
  color: CatColorEnum;

  @Column({ type: 'integer' })
  tail_length: number;

  @Column({ type: 'integer' })
  whiskers_length: number;
}
