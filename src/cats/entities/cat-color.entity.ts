import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class CatColor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: [
      'black',
      'white',
      'black & white',
      'red',
      'red & white',
      'red & black & white',
    ],
  })
  color: string;
}
