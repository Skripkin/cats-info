import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class CatsStat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'numeric', nullable: true })
  tail_length_mean: number;

  @Column({ type: 'numeric', nullable: true })
  tail_length_median: number;

  @Column({ type: 'integer', array: true, nullable: true })
  tail_length_mode: number[];

  @Column({ type: 'numeric', nullable: true })
  whiskers_length_mean: number;

  @Column({ type: 'numeric', nullable: true })
  whiskers_length_median: number;

  @Column({ type: 'integer', array: true, nullable: true })
  whiskers_length_mode: number[];
}
