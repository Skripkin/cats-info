import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CatsStat } from './cats-stat.entity';
import { CatsStatService } from './cats-stat.service';
import { CatsStatController } from './cats-stat.controller';

@Module({
  imports: [TypeOrmModule.forFeature([CatsStat])],
  providers: [CatsStatService],
  controllers: [CatsStatController],
})
export class CatsStatModule {}
