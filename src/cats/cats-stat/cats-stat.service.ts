import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CatsStat } from './cats-stat.entity';

@Injectable()
export class CatsStatService {
  constructor(
    @InjectRepository(CatsStat)
    private readonly catsStatRepository: Repository<CatsStat>,
  ) {}

  async getCatsStat(): Promise<CatsStat> {
    const id = 1;
    return this.catsStatRepository.findOne({ where: { id } });
  }
}
