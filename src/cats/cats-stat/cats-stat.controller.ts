import { Controller, Get } from '@nestjs/common';
import { CatsStatService } from './cats-stat.service';

@Controller('cats-stat')
export class CatsStatController {
  constructor(private readonly catsStatService: CatsStatService) {}

  @Get()
  async getCatsStat() {
    return this.catsStatService.getCatsStat();
  }
}
