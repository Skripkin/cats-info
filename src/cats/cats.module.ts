import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CatsController } from './cats.controller';
import { CatsService } from './cats.service';

import { Cat } from './entities/cats.entity';
import { CatColor } from './entities/cat-color.entity';
import { CatsStat } from './cats-stat/cats-stat.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Cat, CatColor, CatsStat])],
  controllers: [CatsController],
  providers: [CatsService],
})
export class CatsModule {}
