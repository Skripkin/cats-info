import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Cat } from './entities/cats.entity';
import { NewCatDto } from './new-cat-dto.entity';

@Injectable()
export class CatsService {
  constructor(
    @InjectRepository(Cat)
    private readonly catRepository: Repository<Cat>,
  ) {}

  async getColorCounts(): Promise<{ color: string; count: number }[]> {
    const query = `
      SELECT cci.color, COUNT(*) as count
      FROM cats c
      JOIN cat_colors_info cci ON c.color_id = cci.id
      GROUP BY cci.color;
    `;

    try {
      const data = await this.catRepository.query(query);
      return data;
    } catch (error) {
      console.error('Error fetching color counts:', error);
      throw error;
    }
  }

  async getCats(
    attribute?: string,
    order: string = 'asc',
    offset: number = 0,
    limit: number = 10,
  ): Promise<Cat[]> {
    const validAttributes = ['name', 'tail_length', 'whiskers_length', 'color'];

    if (attribute && !validAttributes.includes(attribute)) {
      throw new Error(`Invalid attribute: ${attribute}`);
    }

    if (attribute) {
      const query = `
        SELECT *
        FROM cats
        ORDER BY
          ${attribute} ${order.toUpperCase()}
        OFFSET $1 LIMIT $2;
      `;

      const cats = await this.catRepository.query(query, [offset, limit]);

      return cats;
    } else {
      const query = `
        SELECT *
        FROM cats
        OFFSET $1 LIMIT $2;
      `;

      const cats = await this.catRepository.query(query, [offset, limit]);

      return cats;
    }
  }

  async addCat(newCatData: NewCatDto): Promise<Cat> {
    const existingCat = await this.catRepository.findOne({
      where: {
        name: newCatData.name,
      },
    });
    if (existingCat) {
      throw new Error(`Cat with name ${newCatData.name} already exists.`);
    }

    const newCat = this.catRepository.create({
      ...newCatData,
    });

    try {
      await this.catRepository.save(newCat);
      return newCat;
    } catch (error) {
      console.error('Error adding cat:', error);
      throw error;
    }
  }
}
