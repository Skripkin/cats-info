import { IsNotEmpty, IsString, IsEnum, IsInt } from 'class-validator';

export enum CatColorEnum {
  'black',
  'white',
  'black & white',
  'red',
  'red & white',
  'red & black & white',
}

export class NewCatDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsEnum(CatColorEnum)
  color: CatColorEnum;

  @IsNotEmpty()
  @IsInt()
  tail_length: number;

  @IsNotEmpty()
  @IsInt()
  whiskers_length: number;
}
