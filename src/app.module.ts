import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import { CatsModule } from './cats/cats.module';
import { Cat } from './cats/entities/cats.entity';
import { CatColor } from './cats/entities/cat-color.entity';

import { CatsStat } from './cats/cats-stat/cats-stat.entity';
import { CatsStatModule } from './cats/cats-stat/cats-stat.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'pavel',
      password: '',
      database: 'wg_forge_backend',
      entities: [Cat, CatColor, CatsStat],
      synchronize: true,
    }),
    CatsModule,
    CatsStatModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
